package ru.mail.agb88.test;

/**
 * Created by r2d2 on 30.06.2017.
 */
public interface UserRepository <T> {
    T saveOrUpdate (T t);
    void delete (Integer id);
    T find (Integer id);

}
