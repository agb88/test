package ru.mail.agb88.test.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "T_MEETING")
public class Meeting implements Serializable{

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;

    @OneToMany(mappedBy = "meeting",cascade = CascadeType.ALL)
    private Set <Room> roomSet = new HashSet<>();

    @ManyToMany()
    @JoinTable(name = "T_USER_MEETING",
    joinColumns = @JoinColumn(name = "F_MEETING_ID"),
    inverseJoinColumns = @JoinColumn(name = "F_USER_ID")
    )
    private Set<User> userSet = new HashSet<>();

    public Set<User> getUserSet() {
        return userSet;
    }

    public void setUserSet(Set<User> userSet) {
        this.userSet = userSet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Room> getRoomSet() {
        return roomSet;
    }

    public void setRoomSet(Set<Room> roomSet) {
        this.roomSet = roomSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meeting meeting = (Meeting) o;
        return Objects.equals(id, meeting.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
