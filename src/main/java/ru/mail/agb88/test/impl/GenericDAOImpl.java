package ru.mail.agb88.test.impl;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.mail.agb88.test.util.HibernateUtil;


import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public class GenericDAOImpl<T, ID extends Serializable> {
    Class<T> entity;

    public GenericDAOImpl() {
        this.entity = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Session getSession() {
        return HibernateUtil.getSession();
    }


    public T find(ID id) {
        Transaction transaction = getSession().beginTransaction();
        T load = (T) getSession().load(entity, id);
        transaction.commit();
        return load;
    }

    List<T> findAll() {
        Transaction transaction = getSession().beginTransaction();
        /*Query query = getSession().createQuery("SELECT u from " + entity + " u");
        return query.list();*/
        Criteria criteria = getSession().createCriteria(entity);
        List list = criteria.list();
        transaction.commit();
        return list;
    }

    void save(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().saveOrUpdate(entity);
        transaction.commit();
    }

    void delete(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().delete(entity);
        transaction.commit();
    }
}
