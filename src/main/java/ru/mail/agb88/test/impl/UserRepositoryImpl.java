package ru.mail.agb88.test.impl;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.usertype.ParameterizedType;
import ru.mail.agb88.test.model.User;
import ru.mail.agb88.test.util.HibernateUtil;
import ru.mail.agb88.test.UserRepository;

public class UserRepositoryImpl <T> implements UserRepository<T> {

    private static final Logger logger = Logger.getLogger(UserRepositoryImpl.class);
    private static UserRepositoryImpl instance;
    private Transaction transaction;
    private Class<?> entity;
    private Session session;

    private UserRepositoryImpl() {
        this.entity = getClass();
    }

    public static UserRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new UserRepositoryImpl();
        }
        return instance;
    }

    @Override
    public T saveOrUpdate (T t){
        try{
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            session.saveOrUpdate(t);
            session.getTransaction().commit();
        } catch (HibernateException e){
            logger.error("Error was thrown in DAO: " + e);
            transaction.rollback();
        }

        return t;
    }

    @Override
    public void delete(Integer id) {
        try{
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            T t = (T) session.get(User.class, id);
            session.delete(t);
            transaction.commit();
        } catch (HibernateException e){
            logger.error("Error was thrown in DAO: " + e);
            transaction.rollback();
        }
    }

    @Override
    public T find(Integer id) {
        T t = null;
        try{
            Session session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            t = (T) session.get(User.class, id);
            transaction.commit();
        } catch (HibernateException e){
            logger.error("Error was thrown in DAO: " + e);
            transaction.rollback();
        }
        return t;
    }
}
