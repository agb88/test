package ru.mail.agb88.test;

import org.hibernate.Session;
import ru.mail.agb88.test.impl.UserRepositoryImpl;
import ru.mail.agb88.test.model.*;
import ru.mail.agb88.test.util.HibernateUtil;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = UserRepositoryImpl.getInstance();

        User user = new User();
        user.setUserName("Alexander");

        UserInformation userInformation = new UserInformation();
        userInformation.setAddress("123336 street");

//        Phone phone = new Phone();
//        phone.setPhone_number(7049589);
//
        user.setUserInformation(userInformation);
        userInformation.setUser(user);
//
//        phone.setUser(user);
//        user.getPhones().add(phone);
//
        userRepository.saveOrUpdate(userInformation);

        /*Meeting meeting = new Meeting();
        meeting.setUser_name("Саша");

        Room room = new Room();
        room.setRoom_name("Room 1");
        room.setRoom_number(5);

        room.setMeeting(meeting);
        meeting.setRoom(room);
        session.save(meeting);*/



        /*userRepository.saveOrUpdate(user);
        User user7 =(User) userRepository.find(4);
        System.out.println(user7.getId() + " " + user7.getUserName());
        userRepository.delete(5);*/
    }
}
